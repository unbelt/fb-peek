# FB Peek

Utility extension allowing you to list and see Facebook messages without mark them as seen.

### The application has three sections:

-   list of unreaded messages
-   list of seen messages
-   list of replied messages

> ⚠️ You can only see the last message.
