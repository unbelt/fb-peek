export class Constants {
    static FACEBOOK_URL = 'facebook.com';
    static NO_RESULT_FOUND_MESSAGE = 'No messages found!';
    static WRONG_TAB_MESSAGE = 'Not in Facebook? Please select Facebook tab & expand the Messaging widget';

    static MESSAGE_ITEM_SELECTOR = '.messagesContent';
    static IMAGE_SELECTOR = 'img';
    static SENDER_NAME_SELECTOR = '.author';
    static TIMESTAMP_SELECTOR = '.timestamp';
    static ACTIVE_INDICATOR_SELECTOR = '.presenceActive';
    static SEEN_INDICATOR_SELECTOR = '._5c9_';
    static MESSAGE_CONTENT_SELECTOR = '._1ijj';
    static REPLAY_SELECTOR = '._1ijj > .repliedLast';
}
