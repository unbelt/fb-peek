import { NgModule } from '@angular/core';
import { MatCardModule, MatIconModule, MatTabsModule, MatTooltipModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { MessageCardComponent } from './message-card/message-card.component';

@NgModule({
    declarations: [AppComponent, MessageCardComponent],
    imports: [BrowserModule, BrowserAnimationsModule, MatTabsModule, MatCardModule, MatIconModule, MatTooltipModule],
    bootstrap: [AppComponent],
})
export class AppModule {}
