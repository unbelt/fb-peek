import { Component, OnInit } from '@angular/core';
import { Constants } from './app.constants';
import { IMessage } from './app.models';

@Component({
    selector: 'app-root',
    templateUrl: 'app.html',
    styleUrls: ['app.scss'],
})
export class AppComponent implements OnInit {
    loading: boolean;
    toastr: string;

    unreadMessages: IMessage[];
    seenMessages: IMessage[];
    replies: IMessage[];

    private messageElements: NodeListOf<Element>;
    private timeout: any;

    ngOnInit(): void {
        this.init();

        this.timeout = setTimeout(() => {
            if (this.messageElements && !this.messageElements.length && !this.toastr) {
                this.toastr = Constants.NO_RESULT_FOUND_MESSAGE;
            }
        }, 2000);
    }

    ngOnDestroy = (): void => clearTimeout(this.timeout);

    private init(): void {
        this.loading = true;

        chrome.tabs.query(
            {
                active: true,
                windowId: chrome.windows.WINDOW_ID_CURRENT,
            },
            (tabs: chrome.tabs.Tab[]) => {
                const currentUrl = tabs[0].url;

                if (currentUrl && currentUrl.toLowerCase().indexOf(Constants.FACEBOOK_URL) > -1) {
                    chrome.tabs.executeScript(
                        {
                            code: `(${this.getDOM})();`,
                        },
                        (results: string[]) => {
                            const parser = new DOMParser();
                            const result = parser.parseFromString(results[0], 'text/html');

                            this.messageElements = result.querySelectorAll(Constants.MESSAGE_ITEM_SELECTOR);

                            const messages = this.getMessages();

                            this.unreadMessages = messages.filter(
                                (message: IMessage) => !message.isSeen && !message.isReplay
                            );
                            this.seenMessages = messages.filter(
                                (message: IMessage) => message.isSeen && !message.isReplay
                            );
                            this.replies = messages.filter((message: IMessage) => message.isReplay);

                            this.loading = false;
                        }
                    );
                } else {
                    this.toastr = Constants.WRONG_TAB_MESSAGE;
                    this.loading = false;
                }
            }
        );
    }

    private getDOM = (): string => document.body.innerHTML;

    private getMessages(): IMessage[] {
        const messages: IMessage[] = [];

        this.messageElements.forEach((element: Element) => {
            const imageUrl: HTMLImageElement = element.querySelector(Constants.IMAGE_SELECTOR);
            const name: HTMLElement = element.querySelector(Constants.SENDER_NAME_SELECTOR);
            const content: HTMLElement = element.querySelector(Constants.MESSAGE_CONTENT_SELECTOR);
            const timestamp: HTMLElement = element.querySelector(Constants.TIMESTAMP_SELECTOR);
            const activeIndicator: HTMLElement = element.querySelector(Constants.ACTIVE_INDICATOR_SELECTOR);
            const seenIndicator: HTMLElement = element.querySelector(Constants.SEEN_INDICATOR_SELECTOR);
            const isReplay: HTMLElement = element.querySelector(Constants.REPLAY_SELECTOR);

            const message: IMessage = {
                senderImageUrl: imageUrl && imageUrl.src,
                senderName: name && name.innerText.trim(),
                content: content && content.innerText.trim(),
                date: timestamp && timestamp.title,
                isSenderActive: !!activeIndicator,
                isSeen: !!seenIndicator,
                isReplay: !!isReplay,
            };

            messages.push(message);
        });

        return messages;
    }
}
