export interface IMessage {
    senderImageUrl: string;
    senderName: string;
    date: string;
    content: string;
    isSenderActive: boolean;
    isSeen: boolean;
    isReplay: boolean;
}
